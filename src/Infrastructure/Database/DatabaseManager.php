<?php

namespace App\Infrastructure\Database;

class DatabaseManager
{
    public \PDO $pdo;

    public function __construct()
    {
        $dbHost = getenv('DB_HOST');
        $dbPort = getenv('DB_PORT');
        $dbName = getenv('DB_NAME');
        $dbUser = getenv('DB_USER');
        $dbPassword = getenv('DB_PASSWORD');

        try {
            $this->pdo = new \PDO("mysql:host=$dbHost;port=$dbPort;dbname=$dbName;charset=utf8", $dbUser, $dbPassword);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function read(string $query, string $class, array $criteriaValues = []): array
    {
        $sth = $this->pdo->prepare($query);
        $sth->execute($criteriaValues);

        return $sth->fetchAll(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, $class);
    }

    public function write(string $query, array $values): bool
    {
        $sth = $this->pdo->prepare($query);
        return $sth->execute($values);
    }
}