<?php

namespace App\Infrastructure;

use App\Controller\AuthorController;
use App\Controller\ControllerInterface;

class Routing
{
    public const ROUTES = [
        '/\/authors\/(.*)\/books/' => ['class' => AuthorController::class, 'action' => 'getBooksByAuthorName'],
    ];

    private string $realPath;

    public function __construct($path = null)
    {
        $realPath = $path ?? parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);;
        return $this->realPath = $realPath;
    }

    public function __toString(): string
    {
        $explodedPath = explode('/', $this->realPath);

        $controllerName = 'App\\Controller\\' . ucfirst(substr($explodedPath[1], 0, -1)) . 'Controller';

        if (!class_exists($controllerName)) {
            throw new \Exception('Route for ' . $this->realPath . ' not found');
        }

        /** @var ControllerInterface $controller */
        $controller = new $controllerName;

        // POST Methods
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            return $controller->create();
        } else {
            // check if a route exists
            foreach (Routing::ROUTES as $routePattern => $toLaunch) {
                if (preg_match($routePattern, $this->realPath)) {
                    $class = new $toLaunch['class'];
                    return $class->{$toLaunch['action']}();
                }
            }

            // at least, we consider the second parameter as an id. If there's none, we fetch all entities.
            return isset($explodedPath[2]) ? $controller->getById($explodedPath[2]) : $controller->getAll();
        }
    }
}