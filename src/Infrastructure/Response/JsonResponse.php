<?php

namespace App\Infrastructure\Response;

use App\Infrastructure\ModelInterface;

class JsonResponse implements ResponseInterface
{
    public function __construct(
        private ModelInterface|iterable $toReturn
    ) {}

    public function __toString(): string
    {
        $response = [];
        $response['data'] = $this->toReturn;

        return json_encode($response);
    }
}