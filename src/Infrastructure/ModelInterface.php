<?php

namespace App\Infrastructure;

interface ModelInterface
{
    public function serialize(): array;
}