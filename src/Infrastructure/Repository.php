<?php

namespace App\Infrastructure;

use App\Infrastructure\Database\DatabaseManager;

abstract class Repository
{
    public DatabaseManager $dbm;
    public string $tableName;
    public string $modelName;

    public function __construct(string $modelName)
    {
        $this->dbm = new DatabaseManager();
        $this->tableName = $this->modelNameToTableName($modelName);
        $this->modelName = $modelName;
    }

    public function find(int $id): ModelInterface
    {
        $sql = sprintf('SELECT * FROM %s WHERE id = %d', $this->tableName, $id);

        $result = $this->dbm->read($sql, $this->modelName);

        if (count($result) !== 1) {
            throw new \Exception('One result is expected');
        }

        return $result[0];
    }

    public function findAll(string $order = null, array $criteria = null): array
    {
        $sql = sprintf('SELECT * FROM %s', $this->tableName);

        if($order) {
            $sql .= sprintf(' ORDER BY %s', $order);
        }

        $criteriaValues = [];

        if($criteria) {
            $sql .= ' WHERE ';

            foreach($criteria as $key => $value) {
                $sql .= sprintf('%s LIKE :%s', $key, $key);
                $criteriaValues[$key] = $value;
            }
        }

        return $this->dbm->read($sql, $this->modelName, $criteriaValues);
    }

    public function create(ModelInterface $model): ModelInterface
    {
        $data = $model->serialize();

        $fields = [];
        $values = [];
        $placeholders = [];

        foreach($data as $field => $value) {
            $fields[] = $field;
            $values[] = $value;
            $placeholders[] = '?';
        }

        // Build the SQL query with placeholders
        $sql = sprintf('INSERT INTO %s (%s) values (%s)',
            $this->tableName,
            implode(',', $fields),
            implode(',', $placeholders),
        );
        $this->dbm->write($sql, $values);

        return $this->find($this->dbm->pdo->lastInsertId());
    }

    private function modelNameToTableName(string $modelName): string
    {
        $explodedModelName = explode('\\', $modelName);
        $tableName = end($explodedModelName);

        return mb_strtolower($tableName) . 's';
    }
}
