<?php

namespace App\Domain\User;

use App\Infrastructure\ModelInterface;

class User implements ModelInterface
{
    public int $id;

    public function __construct(
        public string $name,
        public string $email
    ) {}

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }

    public function serialize(): array
    {
        // TODO: Implement serialize() method.
    }
}
