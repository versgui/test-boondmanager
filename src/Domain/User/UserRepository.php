<?php

namespace App\Domain\User;

use App\Infrastructure\Repository;

class UserRepository extends Repository
{
    public function __construct()
    {
        parent::__construct('user');
    }
}
