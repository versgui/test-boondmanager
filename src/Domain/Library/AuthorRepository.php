<?php

namespace App\Domain\Library;

use App\Infrastructure\Repository;

class AuthorRepository extends Repository
{
    public function __construct()
    {
        parent::__construct(Author::class);
    }
}
