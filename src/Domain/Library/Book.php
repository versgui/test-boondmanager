<?php

namespace App\Domain\Library;

use App\Infrastructure\ModelInterface;

class Book implements ModelInterface
{
    public ?int $id = null;
    public ?int $authorId = null;
    public ?string $name = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getAuthorId(): int
    {
        return $this->authorId;
    }

    public function setAuthorId(int $id): self
    {
        $this->authorId = $id;
        return $this;
    }

    public function serialize(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'author' => $this->authorId,
        ];
    }
}
