<?php

namespace App\Domain\Library;

use App\Infrastructure\Repository;

class BookRepository extends Repository
{
    public function __construct()
    {
        parent::__construct(Book::class);
    }

    public function find(int $id): Book
    {
        $sql = sprintf('SELECT books.id, books.name, author.id as authorId FROM books RIGHT JOIN author ON books.author = author.id WHERE books.id = %d', $id);

        $result = $this->dbm->read($sql, $this->modelName);

        if (count($result) !== 1) {
            throw new \Exception('One result is expected');
        }

        return $result[0];
    }
}
