<?php

namespace App\Controller;

use App\Infrastructure\Repository;
use App\Infrastructure\Response\JsonResponse;
use App\Infrastructure\Response\ResponseInterface;

abstract class Controller implements ControllerInterface
{
    public function __construct(
        public Repository $repository,
    ) { }

    public function getById(int $id): ResponseInterface
    {
        $resource = $this->repository->find($id);

        return new JsonResponse($resource);
    }

    public function getAll(): ResponseInterface
    {
        $resources = $this->repository->findAll();

        return new JsonResponse($resources);
    }

    public function checkParameters(array $required): void
    {
        foreach ($required as $requiredValue) {
            if (!array_key_exists($requiredValue, $_POST)) {
                throw new \Exception(sprintf('Parameter %s is missing', $requiredValue));
            }
        }
    }
}
