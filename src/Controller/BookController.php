<?php

namespace App\Controller;

use App\Domain\Library\Book;
use App\Domain\Library\BookRepository;
use App\Infrastructure\Response\JsonResponse;
use App\Infrastructure\Response\ResponseInterface;

class BookController extends Controller implements ControllerInterface
{
    public function __construct()
    {
        parent::__construct(new BookRepository());
    }

    public function create(): ResponseInterface
    {
        $this->checkParameters(['__title__', '__author__']);

        $book = new Book();
        $book->setName($_POST['__title__'])
            ->setAuthorId($_POST['__author__']);

        $books = $this->repository->create($book);

        return new JsonResponse($books);
    }

    public function getAll(): ResponseInterface
    {
        $order = [];
        $queryOrder = null;

        if (isset($_GET['_order_'])) {
            $order['_author_'] = 'author';
            $order['_title_'] = 'name';

            $queryOrder = $order[$_GET['_order_']];
        }

        $resources = $this->repository->findAll($queryOrder);

        return new JsonResponse($resources);
    }
}
