<?php

namespace App\Controller;

use App\Domain\Library\AuthorRepository;
use App\Domain\Library\BookRepository;
use App\Infrastructure\Response\JsonResponse;
use App\Infrastructure\Response\ResponseInterface;

class AuthorController extends Controller implements ControllerInterface
{
    public function __construct()
    {
        parent::__construct(new AuthorRepository());
    }

    public function getBooksByAuthorName(): ResponseInterface
    {
        preg_match('/authors\/(.*)\/books/', $_SERVER['REQUEST_URI'], $found);
        $name = $found[1];
        $order = [];
        $queryorder = null;

        if (isset($_GET['_order_'])) {
            $order['_id_'] = 'id';
            $order['_title_'] = 'name';

            $queryorder = $order[$_GET['_order_']];
        }

        $bookRepository = new BookRepository();
        $resources = $bookRepository->findAll($queryorder, ['name' => $name]);

        return new JsonResponse($resources);
    }
}
