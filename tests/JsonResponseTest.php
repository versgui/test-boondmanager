<?php

namespace Test;

use App\Domain\Library\Author;
use App\Infrastructure\Response\JsonResponse;

class JsonResponseTest extends \PHPUnit\Framework\TestCase
{
    public function testJsonResponse()
    {
        $author = new Author();
        $author->name = 'M. Malzieu';
        $author->id = 1;

        $response = new JsonResponse($author);

        $this->assertJson($response);
    }
}